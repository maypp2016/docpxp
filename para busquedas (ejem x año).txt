-------------
para busquedas (ejem x año)
---------------------
EN VISTA:
----------------------

this.tbarItems = ['-',
            'Gestión:',this.cmbGestion,'-'
        ];

------------------------
PARA MOSTRAR EL AÑO ACTUAL
------------------------
var fecha = new Date();
        Ext.Ajax.request({
            url:'../../sis_parametros/control/Gestion/obtenerGestionByFecha',
            params:{fecha:fecha.getDate()+'/'+(fecha.getMonth()+1)+'/'+fecha.getFullYear()},
            success:function(resp){
                var reg =  Ext.decode(Ext.util.Format.trim(resp.responseText));
                this.cmbGestion.setValue(reg.ROOT.datos.id_gestion);
                this.cmbGestion.setRawValue(fecha.getFullYear());
                this.store.baseParams.id_gestion=reg.ROOT.datos.id_gestion;
                this.load({params:{start:0, limit:this.tam_pag}});
            },
            failure: this.conexionFailure,
            timeout:this.timeout,
            scope:this
        });
		
-----------------------------
this.cmbGestion.on('select', this.capturarEventos, this);
-----------------------------
cmbGestion: new Ext.form.ComboBox({
            //name: 'gestion',
            // id: 'gestion_reg',
            fieldLabel: 'Gestion',
            allowBlank: true,
            emptyText: 'Gestion...',
            blankText: 'Año',
            editable: false,
            store: new Ext.data.JsonStore(
                {
                    url: '../../sis_parametros/control/Gestion/listarGestion',
                    id: 'id_gestion',
                    root: 'datos',
                    sortInfo: {
                        field: 'gestion',
                        direction: 'DESC'
                    },
                    totalProperty: 'total',
                    fields: ['id_gestion', 'gestion'],
                    // turn on remote sorting
                    remoteSort: true,
                    baseParams: {par_filtro: 'gestion'}
                }),
            valueField: 'id_gestion',
            triggerAction: 'all',
            displayField: 'gestion',
            hiddenName: 'id_gestion',
            mode: 'remote',
            pageSize: 5,
            queryDelay: 500,
            listWidth: '280',
            hidden: false,
            width: 80
        }),
------------------------------
 capturarEventos: function () {

            this.store.baseParams.id_gestion = this.cmbGestion.getValue();
            this.load({params: {start: 0, limit: this.tam_pag}});
        },
		
-----------------------------
onButtonNew: function () {
            Ext.Ajax.request({
                url: '../../sis_reclamo/control/Reclamo/getDatosOficina',
                params: {id_usuario: 0},
                success: function (resp) {
                    var reg = Ext.decode(Ext.util.Format.trim(resp.responseText));
                    this.Cmp.id_gestion.setValue(reg.ROOT.datos.id_gestion);
                    this.Cmp.id_gestion.setRawValue(reg.ROOT.datos.gestion);
                    //this.store.baseParams.id_gestion=this.cmbGestion.getValue();
                    this.Cmp.id_gestion.setValue(this.cmbGestion.getValue());
                    this.Cmp.id_gestion.setRawValue(this.cmbGestion.getRawValue());
                },
                failure: this.conexionFailure,
                timeout: this.timeout,
                scope: this
            });

            Phx.vista.EntidadTransferencia.superclass.onButtonNew.call(this);
        },
-------------------------------
EN CONTROL
function listarEntidadTransferencia()
------------------------------
if($this->objParam->getParametro('id_gestion') != ''){
            $this->objParam->addFiltro("ent_tran.id_gestion = ".$this->objParam->getParametro('id_gestion')." ");

        }
