-----------------------------
para que se vea todo el contenido de un campo
-----------------------------
metaData.css = 'multilineColumn';

-------------------
ejemplo
------------------


renderer: function(value, metaData, record, rowIndex, colIndex, store) {
                            
							metaData.css = 'multilineColumn';
                            
							return String.format('{0} <br> {1}', record.data['glosa1'], value);
                        }