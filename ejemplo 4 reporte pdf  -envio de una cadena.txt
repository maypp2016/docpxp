------------------------------
ejemplo 4 reporte pdf 
------------------------------
en control
-----------------------------
function reporteActaConformidadTotal()
    {
        if ($this->objParam->getParametro('id_proceso_wf') != '') {
            $this->objParam->addFiltro("op.id_proceso_wf = " . $this->objParam->getParametro('id_proceso_wf'));
        }

        //para la firma
//var_dump($this->objParam->getParametro('firmar'));exit;
         if ($this->objParam->getParametro('firmar') == 'si') {
       //  if ("conf.fecha_conformidad_final is null") {

        $firmar = 'si';
        $fecha_firma = $this->objParam->getParametro('fecha_firma');
        $usuario_firma = $this->objParam->getParametro('usuario_firma');
        $nombre_usuario_firma = $this->objParam->getParametro('nombre_usuario_firma');
        } else {
            $firmar = 'no';
            $fecha_firma = '';
            $usuario_firma = '';
            $nombre_usuario_firma = '';
        }

        $this->objFunc = $this->create('MODPlanPago');
        $this->res = $this->objFunc->reporteActaConformidadTotal($this->objParam);


        //obtener titulo del reporte
        $titulo = 'Conformidad';
        //Genera el nombre del archivo (aleatorio + titulo)
        $nombreArchivo = uniqid(md5(session_id()) . $titulo);
        $nombreArchivo .= '.pdf';
        $this->objParam->addParametro('orientacion', 'P');
        $this->objParam->addParametro('nombre_archivo', $nombreArchivo);

        //
        $this->objParam->addParametro('firmar', $firmar);
        $this->objParam->addParametro('fecha_firma', $fecha_firma);
        $this->objParam->addParametro('usuario_firma', $usuario_firma);
        $this->objParam->addParametro('nombre_usuario_firma', $nombre_usuario_firma);

        //

        $this->objReporteFormato = new RConformidadTotal($this->objParam);

        //$this->objReporteFormato->setDatos($this->res->datos);
        //$this->objReporteFormato->generarReporte();
        //$firma = $this->objReporteFormato->generarReporte($this->res->getDatos());
        $firma = $this->objReporteFormato->reporteGeneralSegundo($this->res->getDatos());

        $this->objReporteFormato->output($this->objReporteFormato->url_archivo, 'F');

        $this->mensajeExito = new Mensaje();
        $this->mensajeExito->setMensaje('EXITO', 'Reporte.php', 'Reporte generado',
            'Se generó con éxito el reporte: ' . $nombreArchivo, 'control');
        $this->mensajeExito->setArchivoGenerado($nombreArchivo);

        //
        //anade los datos de firma a la respuesta
        if ($firmar == 'si') {
            //$this->mensajeExito->setDatos($this);
            $this->mensajeExito->setDatos($firma);
        }
        //
        $this->mensajeExito->imprimirRespuesta($this->mensajeExito->generarJson());
        //var_dump($firma); exit;
    }
----------------------------------------
en modelo 
-----------------------------------------
function reporteActaConformidadTotal()
    {
        //Definicion de variables para ejecucion del procedimientp
        $this->procedimiento = 'tes.f_plan_pago_sel';
        $this->transaccion = 'TES_ACTCONTOTAL_SEL';
        $this->tipo_procedimiento = 'SEL';//tipo de transaccion
        $this->setCount(false);

        $this->setParametro('id_proceso_wf', 'id_proceso_wf', 'int4');

        //Definicion de la lista del resultado del query

        $this->captura('fecha_inicio', 'varchar');
        $this->captura('fecha_fin', 'varchar');
        $this->captura('fecha_conformidad_final', 'varchar');
        $this->captura('conformidad_final', 'text');
        $this->captura('observaciones', 'varchar');
        $this->captura('numero_tramite', 'varchar');
        $this->captura('proveedor', 'varchar');
        $this->captura('nombre_solicitante', 'text');
        $this->captura('nro_po', 'varchar');
        $this->captura('fecha_po', 'varchar');
        $this->captura('nro_cuota_vigente', 'numeric');
        $this->captura('desc_ingas', 'varchar');
        $this->captura('cantidad_adju', 'numeric');
        $this->captura('descripcion_sol', 'varchar');
        $this->captura('firma', 'varchar');

        //Ejecuta la instruccion
        $this->armarConsulta();
        //echo($this->consulta);exit;
        $this->ejecutarConsulta();
   //var_dump($this->respuesta); exit;

        //Devuelve la respuesta
        return $this->respuesta;
    }
------------------------------------------------
en bd
----------------------------------------------------
     /*********************************
 	#TRANSACCION:  'TES_ACTCONTOTAL_SEL'
 	#DESCRIPCION:	Acta de Conformidad Maestro Plan de Pago Total
 	#AUTOR:			admin
 	#FECHA:			28/08/2018
	***********************************/

	elsif(p_transaccion='TES_ACTCONTOTAL_SEL')then

		begin
        

			--Sentencia de la consulta de conteo de registros
			v_consulta:='select
            			to_char(conf.fecha_inicio,''DD/MM/YYYY'')::varchar,
                        to_char(conf.fecha_fin,''DD/MM/YYYY'')::varchar,
                        to_char(conf.fecha_conformidad_final,''DD/MM/YYYY'')::varchar,
            	        conf.conformidad_final::text,
                        conf.observaciones::varchar,
                        op.num_tramite::varchar,
                        prov.desc_proveedor::varchar as proveedor,
                        fun.desc_funcionario1::text as nombre_solicitante,
                        COALESCE(sol.nro_po, ''S/N'')::varchar,
                        to_char(sol.fecha_po, ''DD/MM/YYYY'')::varchar,
                        op.nro_cuota_vigente::numeric,
                        ci.desc_ingas::varchar,
                        ctd.cantidad_adju::numeric,
                        sold.descripcion::varchar as descripcion_sol,
                        (case when conf.fecha_conformidad_final is not null then ''si'' else ''no'' end)::varchar as firma
                  
                   
                     from tes.tobligacion_pago op 
                     left join tes.tconformidad conf on conf.id_obligacion_pago = op.id_obligacion_pago
                     left join param.vproveedor prov on prov.id_proveedor = op.id_proveedor	
                     left join orga.vfuncionario fun on fun.id_funcionario = op.id_funcionario
                     left join adq.tcotizacion cot on cot.id_obligacion_pago = op.id_obligacion_pago
                     left join adq.tproceso_compra pc on pc.id_proceso_compra = cot.id_proceso_compra
                     left join adq.tsolicitud sol on sol.id_solicitud = pc.id_solicitud
                    
                     left join adq.tcotizacion_det ctd on ctd.id_cotizacion = cot.id_cotizacion
                     left join adq.tsolicitud_det sold on sold.id_solicitud_det=  ctd.id_solicitud_det
                     left join param.tconcepto_ingas ci on ci.id_concepto_ingas = sold.id_concepto_ingas     
                    
                     where op.id_proceso_wf = '||v_parametros.id_proceso_wf ;
                     
			--Definicion de la respuesta
			--v_consulta:=v_consulta||v_parametros.filtro;
            v_consulta:=v_consulta;
     
             raise notice 'consulta %',v_consulta;

			--Devuelve la respuesta
			return v_consulta;

		end;
-------------------------------
en reporte
-----------------------------------
<?php
require_once dirname(__FILE__) . '/../../pxp/lib/lib_reporte/ReportePDFFormulario.php';

//class RConformidadTotal extends ReportePDF
class RConformidadTotal extends ReportePDFFormulario
{
    var $customy;

    function Header()
    {
        $this->ln(15);

//        $x=$this->getX();
//        $y=$this->getY();
        // $this->Image(dirname(__FILE__).'/../../pxp/lib'.$_SESSION['_DIR_LOGO'], $x, $y, 36);
        $this->Image(dirname(__FILE__) . '/../../pxp/lib' . $_SESSION['_DIR_LOGO'], 20, 10, 45);


        $height = 20;
        //cabecera del reporte
        $this->Cell(50, $height, '', 0, 0, 'C', false, '', 0, false, 'T', 'C');
        $this->SetFontSize(16);
        $this->SetFont('', 'B');
        $this->Cell(100, $height, 'ACTA DE CONFORMIDAD FINAL', 0, 0, 'C', false, '', 0, false, 'T', 'C');

        $this->SetMargins(10, 50, 10);
        $this->ln(20);
        $this->customy = $this->getY();
    }

    public function Footer()
    {
        $this->SetFontSize(7);
        $this->setY(-10);
        $ormargins = $this->getOriginalMargins();
        $this->SetTextColor(0, 0, 0);
        //set style for cell border
        $line_width = 0.85 / $this->getScaleFactor();
        $this->SetLineStyle(array('width' => $line_width, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
        $ancho = round(($this->getPageWidth() - $ormargins['left'] - $ormargins['right']) / 3);
        $this->Ln(2);
        $cur_y = $this->GetY();
        //$this->Cell($ancho, 0, 'Generado por XPHS', 'T', 0, 'L');
        $this->Cell($ancho, 0, 'Usuario: ' . $_SESSION['_LOGIN'], '', 1, 'L');
        $pagenumtxt = 'Página' . ' ' . $this->getAliasNumPage() . ' de ' . $this->getAliasNbPages();

        //$this->Cell($ancho, 0, '', '', 0, 'C');
        $fecha_rep = date("d-m-Y H:i:s");
        $this->Cell($ancho, 0, "Fecha impresión: " . $fecha_rep, '', 0, 'L');
        $this->Cell($ancho, 0, $pagenumtxt, '', 0, 'C');
        $this->Ln($line_width);
    }

//    function setDatos($datos)
//    {
//        $this->datos = $datos;
//
//    }


    function reporteGeneralSegundo($maestro)
    {

        $fecha_inicio = $maestro[0]['fecha_inicio'];
        $fecha_fin = $maestro[0]['fecha_fin'];
        $fecha_conformidad_final = $maestro[0]['fecha_conformidad_final'];
        $conformidad_final = $maestro[0]['conformidad_final'];
        $observaciones = $maestro[0]['observaciones'];
        $num_tramite = $maestro[0]['numero_tramite'];
        $proveedor = $maestro[0]['proveedor'];
        $nombre_solicitante = $maestro[0]['nombre_solicitante'];
        $nro_po = $maestro[0]['nro_po'];
        $fecha_po = $maestro[0]['fecha_po'];
        $nro_cuota_vigente = $maestro[0]['nro_cuota_vigente'];
        $desc_ingas = $maestro[0]['desc_ingas'];
        $cantidad_adju = $maestro[0]['cantidad_adju'];
        $descripcion_sol = $maestro[0]['descripcion_sol'];
        $nombre_usuario_firma = $nombre_solicitante;
        $this->firmar = $maestro[0]['firma'];
        //var_dump($nombre_usuario_firma);exit;
        $this->firma['datos_documento']['numero_tramite'] = $num_tramite;
        $this->firma['datos_documento']['nombre_solicitante'] = $nombre_solicitante;
        $this->firma['datos_documento']['proveedor'] = $proveedor;
        $this->firma['datos_documento']['fecha_conformidad_final'] = $fecha_conformidad_final;
        $this->firma['datos_documento']['conformidad_final'] = $conformidad_final;
        $this->firma['datos_documento']['cantidad_adju'] = $cantidad_adju;
$columasconcepto='';
foreach ($maestro as $datomaestro){
    $columasconcepto =$columasconcepto . '<tr>
                     <td width="40%">'.$datomaestro['desc_ingas'].'</td>
                     <td width="40%">'.$datomaestro['descripcion_sol'].'</td>
                     <td width="18%" align="right">'.$datomaestro['cantidad_adju'].'</td>
        		 </tr>';
}
        $this->AddPage();
        $this->SetMargins(10, 50, 10);
        $this->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        //$this->firmar='si';
        $url_firma = $this->crearFirma2();
        //var_dump('gola');exit;

        if (empty($nro_po) and empty($nro_po)) {
           $columanPo='';
        }else{
            $columanPo='  <tr>
            	<td width="50%"> <b>Nro PO:  </b>'.$nro_po.'<br></td> 
            	<td width="50%"> <b>Fecha PO:  </b>'.$fecha_po.'<br></td> 
            </tr>';
        }



            $html = <<<EOF
		<style>
		table, th, td {
   			border: 1px solid black;
   			border-collapse: collapse; 
   			font-family: "Times New Roman";
   			font-size: 11pt;
		}
		</style>
		<body>
		<table border="1">
        	<tr>
            	<td width="65%"><b>Verificado Por:  </b>$nombre_solicitante<br></td> 
            	<td width="35%"> <b>Fecha de Conformidad:  </b>$fecha_conformidad_final<br></td>
            </tr>
        	<tr>
            	<td width="65%"> <b>Número de Trámite:  </b>$num_tramite<br></td> 
            	<td width="35%"> <b>Total Nro Cuota:  </b>$nro_cuota_vigente<br></td> 
            </tr>
        	<tr>
            	<td width="100%"> <b>Proveedor:  </b>$proveedor<br></td> 
            </tr>
            
          $columanPo
            
            <tr>
            	<td width="50%"> <b>Fecha Inicio:  </b>$fecha_inicio<br></td> 
            	<td width="50%"> <b>Fecha Fin:  </b>$fecha_fin<br></td> 
            </tr>
            <tr>
            	<td width="100%"> <b>Conformidad:  </b>$conformidad_final<br></td> 
            </tr>
            <tr>
        	    <td width="100%" align="justify"  colspan="2">
        	    En cumplimiento al Reglamento Específico de las Normas Básicas del Sistema de Administración de Bienes y Servicios de la Empresa,  doy conformidad a lo solicitado.
        	    <br><br>
        	    <table border="0"> 
        	    <tr>
                     <td width="40%" align="center"><b>Concepto</b></td>
                     <td width="40%" align="center"><b>Descripción</b></td>
                     <td width="18%" align="center"><b>Cantidad Adj.</b></td>
                     
        		 </tr>
        	     
        		  $columasconcepto
        	     </table>
            	
            	<br><br>
            	El mismo cumple con las características y condiciones requeridas, en calidad y cantidad. La cuál fue adquirida considerando criterios de economía para la obtención de los mejores precios del mercado.
            	<br><br>
            	En conformidad de lo anteriormente mencionado firmo a continuación:
            	</td>
            </tr>
                   	        	
        	<tr>
            	<td width="100%" align="center"  colspan="2">   <br><br>
            	<img  style="width: 150px;" src="$url_firma" alt="Logo">
            	<br><br>
                $nombre_usuario_firma</td>
        	</tr>
        	
        	<tr>
            	<td width="100%"> <b>Observaciones:  </b>$observaciones<br></td> 
            </tr>
    	</table>
    	</body>
EOF;

        $this->setY($this->customy);
            $this->writeHTML($html);

            return $this->firma;



    }

}

?>