

name:'tipo_reporte',		-NOMBRE	
fieldLabel:'Filtrar por',	-ETIQUETA DE CAMPO
typeAhead: true,			-TIPO DE CABECERA
allowBlank:false,			-PERMITIR EN BLANCO
triggerAction: 'all',		-ACCION DE DISPARO 
emptyText:'Tipo...',		-TEXTO VACIO
selectOnFocus:false,		-SELECCIONAR ENFOQUE
mode:'local',OR 'remote'				-MODO
store:new Ext.data.ArrayStore({		-ALMACENAR
	 fields: ['ID', 'valor'],		-CAMPOS
	  data :	[					-DATOS
		        ['programa','Programa'],
		        ['categoria','Categoría Programática'],	
				['presupuesto','Presupuesto']
				]	        				
	    		}),
	valueField:'ID',				-VALOR CAMPO
	displayField:'valor',			-PANTALLA DE CAMPO
		width:250,					-ANCHURA	
				
			},
type:'ComboBox',			-TIPO
id_grupo:1,					-IDENTIFICADOR DE GRUPO
form:true					-FORMULARIO

		},
----------------------------
qtip: 'la categoria programatica', 		-CONSEJO

store : new Ext.data.JsonStore({		-ALMACENAR
	url:'../../sis_presupuestos/control/CategoriaProgramatica/listarCategoriaProgramatica',
	id : 'id_categoria_programatica',
	root: 'datos',						-RAIZ
	sortInfo:{field: 'codigo_categoria',direction: 'ASC'},		-ORDENAR INFORMACION
	totalProperty: 'total',				-PROPIEDAD TOTAL
	fields: ['codigo_categoria','id_categoria_programatica','descripcion'],
	remoteSort: true,					-ORDEN REMOTO
	baseParams:{par_filtro:'descripcion#codigo_categoria',_adicionar:'si'}	-PARAMETROS BASICOS
}),
hiddenName: 'id_categoria_programatica',		-NOMBRE OCULTO
forceSelection:true,					-SELECCION DE FUERZA
typeAhead: true,						-TIPO DE CABEZA
lazyRender:true,						-PEREZOSO HACER
pageSize:10,							-TAMAÑO DE PAGINA
queryDelay:1000,						-RETRASO DE CONSULTA
listWidth: 280,							-LISTA ANCHO
minChars:2,
tpl:'<tpl for="."><div class="x-combo-list-item"><p>{codigo_categoria}</p><p>{descripcion}</p> </div></tpl>'
			},
			type:'ComboBox',
			id_grupo:1,
			form:true
		},
----------------------------
name: 'nombre_partida',
fieldLabel: 'Nombre Partida',
allowBlank: true,
anchor: '80%',
gwidth: 200,
maxLength:-5							-LONGITUD MAXIMA
	},
type:'TextField',
filters:{pfiltro:'par.nombre_partida',type:'string'},		-FILTROS
bottom_filter: true,					-FILTRO INFERIOR
grid:true,								-CUADRICULA
---------------------------------
renderer:function (value,p,record)				-RENDERIZADOR ->  es una imagen digital que se crea a partir de un modelo o escenario 3D realizado en algún programa de computadora
{return value?value.dateFormat('d/m/Y'):''}
---------------------------------
listWidth: 350							-LISTA ANCHO

--------------------------------
inputType:'hidden',						-tipo de entrada
				